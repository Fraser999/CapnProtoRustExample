//! #CapnProto Example
//! Example of how to use Cap'n Proto with Rust

#![forbid(bad_style, warnings)]
#![deny(missing_docs, deprecated, drop_with_repr_extern, improper_ctypes, non_shorthand_field_patterns,
        overflowing_literals, plugin_as_library, private_no_mangle_fns, private_no_mangle_statics,
        raw_pointer_derive, stable_features, unconditional_recursion, unknown_lints,
        unsafe_code, unsigned_negation, unused, unused_allocation, unused_attributes,
        unused_comparisons, unused_features, unused_parens, while_true)]
#![warn(trivial_casts, trivial_numeric_casts, unused_extern_crates, unused_import_braces,
        unused_qualifications, unused_results, variant_size_differences)]

extern crate capnp;

mod person_capnp {
    include!("../target/generated_capnp_files/schema/person_capnp.rs");
}

/// person
pub use person_capnp::person as Person;
pub use person_capnp::date as Date;
pub use person_capnp::node as Node;
pub use person_capnp::directory as Directory;
pub use person_capnp::file as File;
use capnp::serialize_packed;
use capnp::{MessageBuilder, MallocMessageBuilder};
use std::fs::OpenOptions;
use std::io::BufWriter;

#[allow(dead_code)]
fn main() {
    // Allocate.
    let mut message = MallocMessageBuilder::new_default();
    {
        let mut person = message.init_root::<Person::Builder>();
        // Setters
        person.set_name("Foo Bar Baz");
        person.set_email("foo@bar.baz");
        {
            // Use a Scope to limit lifetime of the borrow.
            let mut birthdate = person.borrow().init_birthdate();
            birthdate.set_day(31u8);
            birthdate.set_month(3u8);
            birthdate.set_year(1973i16);
        }
        {
            // Use a Scope to limit lifetime of the borrow.
            let mut phones = person.borrow().init_phones(2); // 2 phones
            // Unforuntately these borrows must be there.
            phones.borrow().get(0).set_number("(123) 123-1234");
            phones.borrow().get(0).set_type(Person::phone_number::Type::Home);
            phones.borrow().get(1).set_number("(123) 123-1235");
            phones.borrow().get(1).set_type(Person::phone_number::Type::Mobile);
        }
    }

    let target = "target/person.out";
    let file = OpenOptions::new().read(true).write(true).create(true).open(&target).unwrap();
    let mut writer = BufWriter::new(&file);

    let _ = serialize_packed::write_message(&mut writer, &mut message);
    println!("Now try running:\n  capnp decode --packed src/schema/person.capnp Person < {}", target);
}
