extern crate capnpc;
use std::fs;
use std::fs::OpenOptions;
use std::io::Write;
use std::path::PathBuf;

fn main() {
                                                                let mut file = OpenOptions::new().read(true).write(true).create(true).open("target/build.txt").unwrap();
                                                                let _ = writeln!(file, "RUNNING BUILD");
                                                                let _ = writeln!(file, "OUT_DIR:\t\t\t{}", ::std::env::var("OUT_DIR").unwrap());
                                                                let _ = writeln!(file, "TARGET:\t\t\t\t{}", ::std::env::var("TARGET").unwrap());
                                                                let _ = writeln!(file, "HOST:\t\t\t\t{}", ::std::env::var("HOST").unwrap());
                                                                let _ = writeln!(file, "NUM_JOBS:\t\t\t{}", ::std::env::var("NUM_JOBS").unwrap());
                                                                let _ = writeln!(file, "CARGO_MANIFEST_DIR:\t{}", ::std::env::var("CARGO_MANIFEST_DIR").unwrap());
                                                                let _ = writeln!(file, "OPT_LEVEL:\t\t\t{}", ::std::env::var("OPT_LEVEL").unwrap());
                                                                let _ = writeln!(file, "DEBUG:\t\t\t\t{}", ::std::env::var("DEBUG").unwrap());
                                                                let _ = writeln!(file, "PROFILE:\t\t\t{}", ::std::env::var("PROFILE").unwrap());

    let mut outdir: PathBuf = PathBuf::from(::std::env::var("CARGO_MANIFEST_DIR").unwrap());
    outdir.push("target");
    outdir.push("generated_capnp_files");
    std::env::set_var("OUT_DIR", outdir.clone());
                                                                let _ = writeln!(file, "OUT_DIR:\t\t\t{}", ::std::env::var("OUT_DIR").unwrap());

    outdir.push("schema");
                                                                let _ = writeln!(file, "outdir:\t\t\t\t{:?}", outdir.as_path());
    let _ = fs::create_dir_all(outdir.as_path()).unwrap();

    ::capnpc::compile("src", &["src/schema/person.capnp"]).unwrap();
}
